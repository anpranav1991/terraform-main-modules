**THIS README DEMONSTRATES THE BELOW**
1) **HOW TO CREATE THE DOCKERFILE(DOCKERISE) FOR THIS USE-CASE.**
2) **HOW TO CREATE AN IMAGE FROM THE ABOVE DOCKERFILE.**
3) **HOW TO CREATE A CONTAINER FROM THE ABOVE IMAGE.**
4) **HOW TO PUSH THE IMAGE TO REMOTE DOCKER HUB REPOSITORY.**

**HOW TO CREATE THE DOCKERFILE(DOCKERISE) FOR THIS USECASE**
Refer to the file "Dockerfile" in this present working directory.
1) Take Base image as ubuntu:22.04 using FROM command
2) Install the mentioned dependencies curl iproute2 sshfs unzip less groff using RUN command 
3) Install kubectl via curl, make it executable, move it to /usr/local/bin/ and finally run the same using RUN command 
4) Install awscli via curl and run the same using RUN command
5) Finally add all the above installed tools to the PATH /usr/local/bin using ENV command

**HOW TO CREATE AN IMAGE FROM THE ABOVE DOCKERFILE**
1) Navigate to the directory where Dockerfile is present and run the below command.  
**docker build --no-cache -t kubectl_image .**

**--no-cache** => This flag is optional and should be used only if you prefer to rebuild the whole image again.

2) Once the above command is successfully run, then run the below command to check the created image.
docker images -a
![img.png](img.png)

It should list the images like above

**HOW TO CREATE A CONTAINER FROM THE ABOVE IMAGE**  
1) Now, create and start a container out of the above image as below to keep it always up and running  
```docker run -d --name kubectl_container kubectl_image tail -f /dev/null```

2) Now, run the below command to check the list of containers  
```docker ps -a```

![img_1.png](img_1.png)

you should see the container up and running as above

3) Optionally, at this point you can also exec into the container and check whether the packages are properly installed.
![img_3.png](img_3.png)



**HOW TO PUSH THE IMAGE TO REMOTE DOCKER HUB REPOSITORY**
To upload the Docker image to Docker Hub, we need to follow these steps
1) Tag the Docker image with the Docker Hub username and the desired image name. The format for the tag should be   
```docker tag kubectl_image <dockerhub_username>/<image>:latest```

2) Now, login to your dockerhub using docker login command  
```docker login```

Enter the username and password, post which it returns "Login Succeeded!"

3) Now, you can push the docker image to your docker hub repository using the below command  
```docker push kubectl_image <dockerhub_username>/<image>:latest```

4) That's when you can see the image uploaded to the docker hub repository as below.  
![img_2.png](img_2.png)




