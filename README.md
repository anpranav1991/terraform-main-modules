I Have explained the whole assignment in the below video with explanation. Please find the link

https://drive.google.com/file/d/1uLIXZpVH85sVxf4dIXh_wMVKElXOgnIA/view?usp=drive_link

![img.png](img.png)

Created and Deployed the above mentioned architecture in even more better and secure way.  
To achieve the above in AWS, we need a lot of intermediate resources to achieve consistency, efficiency and security.

Below is the recommended AWS 3 tier architecture for reference which i was talking in the video link above. This architecture
includes the asked architecture above anyways.

![img_16.png](img_16.png)


Total Resources Created
Plan: 46 to add, 0 to change, 0 to destroy.

1 Cloudwatch Log Group  
3 Elastic IPs  
1 aws flowlogs for ec2 instance  
1 IAM ROLE  
1 IAM ROLE POLICY   
3 EC2 Instances in private subnets 
1 Internet Gateway  
1 AWS keypair
1 AWS KMS Key  
3 AWS NAT Gateways  
4 Route Tables  
6 Route Table Associations  
1 S3 Bucket for Logging the main bucket  
1 S3 Bucket Main  
2 S3 bucket ACLs  
1 S3 Bucket Logging Resource  
2 S3 Bucket Ownership Controls  
2 S3 Bucket Block Public Access Resources  
1 Security Group  
3 Public Subnets(These are required for ALB's etc which are public facing, although not required for this usecase)  
6 Private Subnets(3 private subnets for EC2 instances that will be put in private subnets  
and another 3 for RDS etc although not required now, but this is best practise to have separate
private subnets for RDS and ec2 instances)  
1 VPC  
1 tls key for keypair    



These resources complete the whole architecture and any possible scaling from here is definetly easily accomplished
once the above networking is created as above.


GITLAB PIPELINE:
You can check that all the stages passed and the whole infra has been created by gitlab CICD Pipeline
1) ![img_1.png](img_1.png)
![img_2.png](img_2.png)

You can check that the initialisation happened without any issues

2) ![img_3.png](img_3.png)

Terrascan has validated and scanned with 0 errors and 0 vulnerabilities

3) Terraform Validated Successfully
![img_4.png](img_4.png)


4) Terraform Plan is Successfull  
![img_7.png](img_7.png)

5) Terraform Applied successfully
![img_8.png](img_8.png)

6)Optionally, with a manual approval, i could successfully destroy the resources as well

![img_5.png](img_5.png)
![img_6.png](img_6.png)


You can check all the pipelines, in the Gitlab Pipelines section.


Screenshots of Major resources created

1) 3 EC2 Instances based on number of AZs
![img_14.png](img_14.png)
2) 1 VPC
![img_10.png](img_10.png)
3) 1 S3 Bucket
![img_11.png](img_11.png)











