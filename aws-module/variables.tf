# General Variables
variable "region" { default = "eu-west-1" }
variable "organization" { default = "intuitive" }
variable "env" { default = "dev" }
variable "instance_type" { default = "t2.medium" }
variable "volume_size" { default = "50" }
variable "Environment" { default = "dev" }
variable "service_name" { default = "interview" }
variable "volume_type" { default = "gp2" }
variable "ami_id" { default = "ami-0cc4e06e6e710cd94" }
variable "security_list" {
  type = list(any)
  default = [
    {
      from_port = "80"
      to_port   = "80"
    },
    {
      from_port = "443"
      to_port   = "443"
    }
  ]
}
variable "protocol" { default = "TCP" }
variable "open_cidr" { default = "49.204.208.124/32" }

# Network Variables
variable "cidr_block" { default = "192.168.0.0/16" }
variable "additional_tags" {
  type    = map(any)
  default = {}
}

variable "subnet_tier1_prefix" { default = "7" }
variable "subnet_tier1_range" { default = "1" }
variable "subnet_tier2_prefix" { default = "5" }
variable "subnet_tier2_range" { default = "2" }
variable "subnet_tier3_prefix" { default = "7" }
variable "subnet_tier3_range" { default = "5" }
variable "add_tags_enabled" { default = "false" }
variable "env_select" {
  type = map(any)
  default = {
    "stage" = "stg"
    "dev"   = "dev"
    "prod"  = "prd"
  }
}